# Questions

> What would you add to your solution if you had more time?

I would see if the tests could be improved or expanded. Then, I would consider the performance implications of consuming a stream of data from a web socket. I'd also explore the possible error responses from the web socket endpoint and provide a more curated error experience. Lastly, I would consider if any more UI/behavioral logical can be abstracted away.

> What would you have done differently if you knew this page was going to get thousands of views
> per second vs per week?

Since this page is served as a static bundle, I would host the application a platform that has a global CDN so that the page assets can be loaded effectively. If the audience is large enough (global), we could build in i18n & right-to-left language support.

> What was the most useful feature that was added to the latest version of your chosen language?
> Please include a snippet of code that shows how you've used it.

I find that [template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) added in [ES2015](https://262.ecma-international.org/6.0/#sec-template-literals) to be the mose useful new feature I used in this project. I enjoy that it allows developers to embed JavaScript expressions into strings. I find that template literals allows strings to be much more flexible.

One example of how this was used was to create the background color of each level in the order book. Template literals allowed for expressiveness (rather than manual string concatenation).

```js
const [price, size, total] = ask;
const percent = (total / arr[0][2]) * 100;
const background = `linear-gradient(to right, rgba(255, 90, 106, 0.3) ${percent}%, transparent 0%)`;
```

> How would you track down a performance issue in production? Have you ever had to do this?

Yes, I have had to track down performance issues. First, I determine the type of performance issue I'm encountering. Is it a network, rendering, memory, etc, issue? Once the type of performance issue is determined there are various tools to help investigate further.

For network issues this includes Chrome's network panel tooling. This tool can add visibility into network payloads and caching issues. Chrome's Performance tab produces a flame graph that provides one insight into the callstack and how each function and module performs. The React profiler is a great tool for debugging rendering performance issues in React.

One of the best ways to track down performance issues in React applications is to analyze React components and their props. A common performance problem is that reference values are passed as props to components that have expensive rendering. Some common techniques to tackle rendering performance: memoization, windowing, debouncing, request animation frame, etc.

Lastly, CSS that can take advantage of the compositor thread (with the help of the GPU) can improve browser layout & painting performance. For example, `transform: translateX()` is more efficient than the `left` CSS property.

> Can you describe common security concerns to consider for a frontend developer?

User submitted input is a common security concern for frontend developers. To prevent injection attacks a common technique is the sanitize all user input before passing it along to backend services.

A browser environment is typically safe because it is a sandboxed environment. In modern frontend development, a larger security concern is the third party scripts we include in our web applications. These include third party services (marketing, pixels, tracking, etc) and third party JavaScript (npm) modules. As a frontend developer, one should be mindful anytime a third party script is included in an application.

> How would you improve the Kraken API that you just used?

I think the [API docs](https://support.cryptofacilities.com/hc/en-us/articles/360000538773-Book) could include fully typed objects. Also, documenting error types and its structure would be helpful for frontend developers. In my project, I provide the user with a generic error message. Given more information of possible error responses and shapes, I could tailor the error UI to a particular error.

⭐ Thanks for taking the time to review this project!
