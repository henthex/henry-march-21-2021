import { useState, useEffect } from "react";
import { Bid, Ask, PriceLevels, ProductIds } from "../types";

const WEB_SOCKET_API = "wss://www.cryptofacilities.com/ws/v1";

const useOrderBook = (
  productIds: ProductIds[],
  onError: (e: Event) => void
) => {
  const [bids, setBids] = useState<PriceLevels>([]);
  const [asks, setAsks] = useState<PriceLevels>([]);

  useEffect(() => {
    const ws = new WebSocket(WEB_SOCKET_API);

    const subscription = {
      event: "subscribe",
      feed: "book_ui_1",
      product_ids: productIds,
    };

    ws.onopen = () => {
      ws.send(JSON.stringify(subscription));
    };

    ws.onmessage = (e) => {
      const hasSize = (order: Bid | Ask) => order[1] !== 0;
      const newBids: PriceLevels = JSON.parse(e.data).bids || [];    
      const newAsks: PriceLevels = JSON.parse(e.data).asks || [];

      setBids((prevBids) => [...prevBids, ...newBids.filter(hasSize)]);
      setAsks((prevAsks) => [...prevAsks, ...newAsks.filter(hasSize)]);
    };

    ws.onclose = () => {
      ws.close();
    };

    ws.onerror = (e) => onError(e)

    return () => {
      // Cleanup subscription(s) when component unmounts
      ws.close();
    };
  }, [onError, productIds]);

  return { bids, asks };
};

export default useOrderBook