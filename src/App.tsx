import { useState, useCallback } from "react";
import Error from "./components/Error";
import OrderBook from "./components/OrderBook";
import LoadingIndicator from "./components/LoadingIndicator";
import useOrderBook from "./hooks/useOrderBook";
import { ProductIds } from "./types";

function App() {
  const [error, setError] = useState(false);
  const [productIds, setProductIds] = useState<ProductIds[]>(["PI_XBTUSD"]);
  const onError = useCallback((error) => {
    // Optionally record error to an error tracking service like Rollbar or Sentry
    setError(true);
  }, []);
  const { bids, asks } = useOrderBook(productIds, onError);

  return (
    <div className="bg-gray-50">
      {error ? <Error /> : null}

      <div className="flex pt-4 pb-8 justify-center">
        <label htmlFor="market-select" className="mr-2">
          Select market:
        </label>
        <select
          name="markets"
          id="market-select"
          className="border border-gray-500	rounded-sm"
          onChange={(e) => setProductIds([e.target.value as ProductIds])}
        >
          <option value="PI_XBTUSD">XBT/USD</option>
          <option value="PI_ETHUSD">ETH/USD</option>
        </select>
      </div>

      <div className="flex justify-center items-start h-screen">
        {bids.length || asks.length ? (
          <OrderBook bids={bids} asks={asks} />
        ) : (
          <LoadingIndicator />
        )}
      </div>
    </div>
  );
}

export default App;
