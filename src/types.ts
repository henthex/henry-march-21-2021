type Price = number

type Size = number

export type Ask = [Price, Size];

export type Bid = [Price, Size];

export type PriceLevels = Ask[] | Bid[]

export type ProductIds = "PI_XBTUSD" | "PI_ETHUSD";
