const Error = () => (
  <div className="text-gray-800 text-center p-3 text-lg bg-red-200">
    An error has occured. Please{" "}
    <a
      href={window.location.href}
      className="underline text-blue-600 hover:text-blue-800 visited:text-purple-600 cursor-pointer"
    >
      refresh
    </a>{" "}
    the page or try again later.
  </div>
);

export default Error;
