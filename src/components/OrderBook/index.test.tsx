import { render, screen } from "@testing-library/react";
import OrderBook from "./index";
import { PriceLevels } from "../../types";

const bids: PriceLevels = [
  [58366.5, 109400],
  [58366, 109400],
  [58364, 15000],
  [58359.5, 109400],
  [58359, 7631],
  [58358.5, 32155],
  [58358, 20000],
  [58357, 22106],
  [58356.5, 30000],
  [58355.5, 109400],
  [58352.5, 22599],
];

const asks: PriceLevels = [
  [58262, 9071],
  [58285.5, 10000],
  [58287.5, 38869],
  [58288, 17000],
  [58294.5, 21018],
  [58301.5, 75343],
  [58302, 10000],
  [58306.5, 200000],
  [58307.5, 3518],
  [58311, 24044],
  [58318, 200],
];

describe("OrderBook", () => {
  describe("Asks", () => {
    test("levels are sorted by price (descending)", () => {
      render(<OrderBook asks={asks} bids={bids} />);

      const sortedPrices = asks
        .map((ask) => ask[0])
        .sort((first, second) => second - first)
        .map((price) =>
          price.toLocaleString(undefined, {
            minimumFractionDigits: 2,
          })
        );
      const tdNodes = screen.getAllByTestId("ask-price");

      tdNodes.forEach((td, index) => {
        expect(td.textContent).toBe(sortedPrices[index]);
      });
    });

    test("shows correct totals for each level", () => {
      render(<OrderBook asks={asks} bids={bids} />);

      const totals = asks
        .slice(bids.length - 10)
        .sort((first, second) => second[0] - first[0])
        .map((ask, index, list) => {
          const total = list.reduce(
            (acc, curr, rIndex) => (index <= rIndex ? acc + curr[1] : acc),
            0
          );

          return total.toLocaleString();
        });
      const tdNodes = screen.getAllByTestId("ask-total");

      tdNodes.forEach((td, index) => {
        expect(td.textContent).toBe(totals[index]);
      });
    });
  });
  describe("Bids", () => {
    test("levels are sorted by price (descending)", () => {
      render(<OrderBook asks={asks} bids={bids} />);

      const sortedPrices = bids
        .slice(bids.length - 10)
        .map((bid) => bid[0])
        .sort((first, second) => {
          return second - first;
        })
        .map((price) =>
          price.toLocaleString(undefined, {
            minimumFractionDigits: 2,
          })
        );

      const tdNodes = screen.getAllByTestId("bid-price");

      tdNodes.forEach((td, index) => {
        expect(td.textContent).toBe(sortedPrices[index]);
      });
    });

    test("shows correct totals for each level", () => {
      render(<OrderBook asks={asks} bids={bids} />);

      const totals = bids
        .slice(bids.length - 10)
        .sort((first, second) => second[0] - first[0])
        .map((bid, i, list) => {
          const total = list.reduce(
            (acc, curr, rIndex) => (i >= rIndex ? acc + curr[1] : acc),
            0
          );

          return total.toLocaleString();
        });

      const tdNodes = screen.getAllByTestId("bid-total");

      tdNodes.forEach((td, index) => {
        expect(td.textContent).toBe(totals[index]);
      });
    });
  });
});
