import React from "react";
import { PriceLevels } from "../../types";

interface IOrderBookProps {
  bids: PriceLevels;
  asks: PriceLevels;
  rowCount?: number;
}

const PRICE_INFO =
  "Bids - The bid price represents the maxium price that a user is willing to buy at. Asks - the ask price represents the minimum price a user is willing to sell at";

const SIZE_INFO = "Number of contracts available at this price level";

const TOTAL_INFO = "Cumulative number of contracts at this price level";

const OrderBook: React.FC<IOrderBookProps> = ({
  bids,
  asks,
  rowCount = 10,
}) => {
  return (
    <div className="text-white bg-gray-800 rounded-sm">
      <table style={{ minWidth: "330px" }}>
        <caption className="sr-only">Asks</caption>
        <thead>
          <tr className="text-right text-gray-300">
            <th title={PRICE_INFO} className="py-1 px-2 font-medium">
              Price
            </th>
            <th title={SIZE_INFO} className="py-1 px-2 font-medium">
              Size
            </th>
            <th title={TOTAL_INFO} className="py-1 px-2 font-medium">
              Total
            </th>
          </tr>
        </thead>
        <tbody className="text-right">
          {asks
            .slice(asks.length - rowCount)
            .sort((first, second) => second[0] - first[0])
            .map((ask, i, list) => {
              const total = list.reduce(
                (acc, curr, rIndex) => (i <= rIndex ? acc + curr[1] : acc),
                0
              );

              return [...ask, total];
            })
            .map((ask, i, arr) => {
              const [price, size, total] = ask;
              const percent = (total / arr[0][2]) * 100;
              const background = `linear-gradient(to right, rgba(255, 90, 106, 0.3) ${percent}%, transparent 0%)`;

              return (
                <tr key={i} style={{ background }}>
                  <td
                    className="text-red-500 py-1 px-2 w-2/6"
                    data-testid="ask-price"
                  >
                    {price.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                    })}
                  </td>
                  <td className="py-1 px-2 w-2/6">{size.toLocaleString()}</td>
                  <td className="py-1 px-2 w-2/6" data-testid="ask-total">
                    {total.toLocaleString()}
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
      <table style={{ minWidth: "330px" }}>
        <caption className="sr-only">Bids</caption>
        <thead className="sr-only">
          <tr>
            <th>PRICE</th>
            <th>SIZE</th>
            <th>TOTAL</th>
          </tr>
        </thead>
        <tbody className="text-right">
          {bids
            .slice(bids.length - rowCount)
            .sort((first, second) => second[0] - first[0])
            .map((bid, i, list) => {
              const total = list.reduce(
                (acc, curr, rIndex) => (i >= rIndex ? acc + curr[1] : acc),
                0
              );

              return [...bid, total];
            })
            .map((bid, i, arr) => {
              const [price, size, total] = bid;
              const percent = (total / arr[rowCount - 1][2]) * 100;
              const background = `linear-gradient(to right, rgba(41,186,165, 0.3) ${percent}%, transparent 0%)`;

              return (
                <tr key={i} style={{ background }}>
                  <td
                    className="py-1 px-2 w-2/6 text-green-500"
                    data-testid="bid-price"
                  >
                    {price.toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                    })}
                  </td>
                  <td className="py-1 px-2 w-2/6">{size.toLocaleString()}</td>
                  <td className="py-1 px-2 w-2/6" data-testid="bid-total">
                    {total.toLocaleString()}
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};

export default OrderBook;
