import "./index.css";

const LoadingIndicator = () => <div className="loader">Loading...</div>;

export default LoadingIndicator;
