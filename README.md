# order-book

This project implements a simple order book user interface for crypto markets. A live demo can be found [here](https://order-book.netlify.app/).

## Quick start

Install project dependencies & start application with a single command:

```
yarn install && yarn start
```

Open [http://localhost:3000](http://localhost:3000) to view application in the browser.

## Overview

### Styling

This project uses [Tailwind CSS](https://tailwindcss.com/) for styling. Tailwind offers a set of utility classes that can be composed to form visual elements. Unused utility classes are purged using tree-shaking.

### Testing

Run `yarn test` to launch the test runner in the interactive watch mode. A minimal set of tests can be found in `/src/components/OrderBook/index.test.tsx`. Testing is one of my favorite areas of development. Here are some of my [musings](https://paper.dropbox.com/doc/On-Software-Testing--BHUdWouF~RO~9hLn9fDT1Iv7Ag-wtVlTcftrHTLU8zVhZIhv) on testing.
